#!/bin/bash

set -ex

sudo apt update -qq \
     && sudo apt upgrade -y \
     && sudo apt install -y curl git htop screenfetch

curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

curl -sSL https://repos.insights.digitalocean.com/install.sh | sudo bash