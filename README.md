# README 

This repository contains BASH scripts to perform initial setup of various Linux server distributions.

### What is this repository for? ###

* Fast setup of bare metal Linux installations.
* Docker services included.

### How do I get set up? ###

* Download the appropriate script to server using CURL
* Execute the script using sudo